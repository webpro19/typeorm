import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productRopository = AppDataSource.getRepository(Product);
    // const product = new Product()
    // product.name = "พระจันทร์มันไก่";
    // product.price = 50;
    // await productRopository.save(product)
    // console.log("Saved a new user with id: " + product.id)

    // console.log("Loading products from the database...")
    const products = await productRopository.find()
    console.log("Loaded products: ", products)

    const updateProduct = await productRopository.findOneBy({id:1})
    console.log(updateProduct)
    updateProduct.price=80;
   await productRopository.save(updateProduct);

 
}).catch(error => console.log(error))

